console.log("Hello World");


// Mock Database

let posts = [];

// Post ID

let count = 1;

// Add Post

document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	e.preventDefault();


	posts.push({
		id: count,
		title: document.querySelector("#txt-title").value,
		body: document.querySelector("#txt-body").value
	});

	count++
	console.log(posts);
	alert("Successfully added!");
	// invoke a function to show the post later
	showPost();

});


// Edit Post 

const showPost = () => {
	let postEntries = "";

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>
			</div>
		`
	});
	console.log(postEntries);
	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// Edit Post Button 


const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

};


// Update Post

document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();

	for (let i = 0; i < posts.length; i ++) {

		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
			posts[i].title = document.querySelector("#txt-edit-title").value,
			posts[i].body = document.querySelector("#txt-edit-body").value

			showPost(posts);
			alert("Successfully Updated!");
			break;
		};
	};
});


// s48 Activity
	/*
		1. Add a new function in index.js called deletePost().
		2. This function should be able to receive the id number of the posts.
			a. Remove the item with the same id number from the posts array
			b. Note: you can use array methods such as filter() or splice()
			c. Then, remove the element from the DOM by first selecting the elements and using the remove() method
	*/

const deletePost = (id) => {
  let element = document.getElementById(`post-${id}`);
  element.remove();
};


/*
	const deletePost = (id) => {
		posts = posts.filter((post) => {
			if(post.id != id) {
				return post
			};
		});
		console.log(posts);
		document.querySelector(`#post-${id}`).remove();
	};
*/


/*
	const deletePost = (id) => {
		for (let i = 0; i < posts.length; i++) {
			if(posts[i].id.toString() === id) {
				posts.splice(i, 1);
			};
		};
		showPost(posts);
		alert("Post is deleted!");
	}
*/

/*
	const deletePost = (id) => {
		posts.splice(id-1, 1);
		showPost();
	}
*/

/*
	const deletePost = (id) => {
		document.querySelector(`#post-${id}`).remove();
	}
*/